# Technical Test 

	1.User Registration
	2.Slack Integration


### Prerequisites

	php: ^7.0
	laravel/framework: 5.6.*
	anhskohbo/no-captcha : ^3.0
	freshbitsweb/slack-error-notifier : ^1.0
